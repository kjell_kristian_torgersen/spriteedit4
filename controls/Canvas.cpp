/*
 * Canvas.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "Canvas.h"
#include "global.h"
#include <iostream>
#include "RectBrush.h"
#include "CircleBrush.h"
#include "FillCommand.h"
#include "RandomCommand.h"
#include "ICommand.h"

#define G 16

Canvas::Canvas(sf::Vector2u pos, Bitmap& bmp) :
		pos(pos), size( { 16 * 32, 16 * 32 }), drawing(false), palette( { pos.x + size.x, pos.y }), bmp(bmp), brush(new RectBrush(bmp, 1, 1))
{
	for (int y = 0; y < 32; y++) {
		for (int x = 0; x < 32; x++) {
			sf::RectangleShape rs;
			rs.setPosition(G * x, G * y);
			rs.setOutlineThickness(1);
			rs.setOutlineColor(sf::Color::Black);
			rs.setFillColor(sf::Color::White);
			rs.setSize( { G, G });
			rss.push_back(rs);
		}
	}
}

void Canvas::Draw(sf::RenderWindow& window)
{
	sf::RectangleShape rs;

	rs.setOutlineThickness(1);
	rs.setOutlineColor(sf::Color::Black);
	rs.setFillColor(sf::Color::White);
	rs.setSize( { G, G });

	for (unsigned y = 0; y < bmp.getH(); y++) {
		for (unsigned x = 0; x < bmp.getW(); x++) {
			rs.setPosition(pos.x + G * x, pos.y + G * y);
			int col = (int) bmp.operator ()(x, y);
			rs.setFillColor(GetSfColor(col));
			window.draw(rs);
		}
	}
	palette.Draw(window);
	sf::Texture txt;
	txt.loadFromImage(bmp.ToImg());
	sf::Sprite sprite(txt);

	for (int y = 0; y < 3; y++) {
		for (int x = 0; x < 3; x++) {
			sprite.setPosition(pos.x + size.x + palette.GetSize().x + 32 * x, pos.y + 32 * y);
			window.draw(sprite);
		}
	}

}

void Canvas::MouseClicked(int x, int y)
{
	int mx = (x - pos.x) / G;
	int my = (y - pos.y) / G;

	palette.MouseClicked(x, y);
	brush->SetColor(palette.GetColor());
	if (mx >= 0 && mx < 32 && my >= 0 && my < 32) {
		bmp.Set();
		brush->Draw(mx, my);
	}
	drawing = true;

}

void Canvas::MouseReleased(int x, int y)
{
	drawing = false;
}

void Canvas::MouseMoved(int x, int y)
{
	if (drawing) {
		int mx = (x - pos.x) / G;
		int my = (y - pos.y) / G;
		if (mx >= 0 && mx < 32 && my >= 0 && my < 32) {
			brush->Draw(mx, my);
		}
	}
}

void Canvas::KeyPressed(sf::Keyboard::Key key, int x, int y)
{
	switch (key) {
	case sf::Keyboard::V:
		bmp.Set();
		for (unsigned i = 0; i < bmp.getH(); i++) {
			brush->Draw((x - pos.x) / 16, i);
		}
		break;
	case sf::Keyboard::H:
		bmp.Set();
		for (unsigned i = 0; i < bmp.getW(); i++) {
			brush->Draw(i, (y - pos.y) / 16);
		}
		break;
	case sf::Keyboard::U:
		bmp.Undo();
		break;
	case sf::Keyboard::C:
		bmp.Clear(17);
		break;
	case sf::Keyboard::D:
		bmp.Double();
		break;
	case sf::Keyboard::O:
		//bmp.Offset(bmp.getW()/2, bmp.getH()/2);
		bmp.Offset(4, 4);
		break;
	case sf::Keyboard::F: {
		unsigned tx = (x - pos.x) / 16;
		unsigned ty = (y - pos.y) / 16;
		if (tx < 32 && ty < 32) {
			std::cout << "tx: " << tx << " ty: " << ty << std::endl;
			Execute(new FillCommand(tx, ty, palette.GetColor()));
		}
	}
		break;
	case sf::Keyboard::R: {
		Execute(new RandomCommand(palette.GetColor()));
	}
		break;
	case sf::Keyboard::Up:
		bmp.Offset(0, 1);
		break;
	case sf::Keyboard::Down:
		bmp.Offset(0, -1);
		break;
	case sf::Keyboard::Left:
		bmp.Offset(1, 0);
		break;
	case sf::Keyboard::Right:
		bmp.Offset(-1, 0);
		break;
	default:
		break;
	}
}

void Canvas::KeyReleased(sf::Keyboard::Key key, int x, int y)
{
}

sf::Vector2u& Canvas::GetSize()
{
	return size;
}

sf::Vector2u& Canvas::GetPos()
{
	return pos;
}

void Canvas::SetBrush(IBrush* brush)
{
	if (this->brush != nullptr) {
		delete this->brush;
	}
	this->brush = brush;
}

void Canvas::Execute(ICommand* cmd)
{
	cmd->Execute(bmp);
	delete cmd;
}

Canvas::~Canvas()
{

}

