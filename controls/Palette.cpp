/*
 * Palette.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "Palette.h"
#include "global.h"

Palette::Palette(sf::Vector2u pos) :
		pos(pos), size( { 32, 17 * 32 }), drawColor(0)
{

}

void Palette::Draw(sf::RenderWindow& window)
{

	for (int i = 0; i < 17; i++) {
		sf::RectangleShape rs;
		rs.setFillColor(GetSfColor(i));
		rs.setPosition(pos.x, pos.y + 32 * i);
		rs.setSize( { 32, 32 });
		if (i == drawColor) {
			rs.setOutlineThickness(2);
		} else {
			rs.setOutlineThickness(0);
		}
		window.draw(rs);
	}
}

void Palette::MouseClicked(int x, int y)
{
	unsigned rx = (x - pos.x);
	unsigned ry = (y - pos.y);
	if (rx < size.x && ry < size.y) {
		drawColor = ry / 32;
	}
}

void Palette::MouseReleased(int x, int y)
{
}

void Palette::MouseMoved(int x, int y)
{
}

void Palette::KeyPressed(sf::Keyboard::Key key, int x, int y)
{
}

void Palette::KeyReleased(sf::Keyboard::Key key, int x, int y)
{
}

sf::Vector2u& Palette::GetSize()
{
	return size;
}

sf::Vector2u& Palette::GetPos()
{
	return pos;
}

Palette::~Palette()
{
	// TODO Auto-generated destructor stub
}

int Palette::GetColor()
{
	return drawColor;
}

