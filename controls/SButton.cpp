/*
 * SButton.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "SButton.h"
#include "global.h"

SButton::SButton(sf::Vector2u pos, sf::Vector2u size, std::string text) : pos(pos), size(size), text(text)
{
	rs.setOrigin(pos.x, pos.y);
	rs.setSize(sf::Vector2f(size.x, size.y));
	rs.setOutlineColor(sf::Color::Black);
	rs.setOutlineThickness(1);
}

void SButton::Draw(sf::RenderWindow& window)
{
	window.draw(rs);
}

void SButton::MouseClicked(int x, int y)
{
}

void SButton::MouseReleased(int x, int y)
{
}

void SButton::MouseMoved(int x, int y)
{
	unsigned px = x - pos.x;
	unsigned py = y - pos.y;
	if (px < size.x && py < size.y) {
		rs.setFillColor(GetSfColor(7));
		rs.setOutlineThickness(2);
	} else {
		rs.setFillColor(GetSfColor(8));
		rs.setOutlineThickness(1);
	}
}

void SButton::KeyPressed(sf::Keyboard::Key key, int x, int y)
{
}

void SButton::KeyReleased(sf::Keyboard::Key key, int x, int y)
{
}

sf::Vector2u& SButton::GetSize()
{
}

sf::Vector2u& SButton::GetPos()
{
}
