/*
 * IBrush.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef IBRUSH_H_
#define IBRUSH_H_

class IBrush {
public:
	virtual void SetColor(unsigned int color)=0;
	virtual void Draw(unsigned int x, unsigned int y)=0;
	virtual ~IBrush()
	{
	}
};

#endif /* IBRUSH_H_ */
