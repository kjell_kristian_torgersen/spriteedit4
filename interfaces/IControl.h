/*
 * IControl.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef ICONTROL_H_
#define ICONTROL_H_

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class IControl {
public:
	virtual void Draw(sf::RenderWindow& window)=0;
	virtual void MouseClicked(int x, int y)=0;
	virtual void MouseReleased(int x, int y)=0;
	virtual void MouseMoved(int x, int y)=0;
	virtual void KeyPressed(sf::Keyboard::Key key, int x, int y)=0;
	virtual void KeyReleased(sf::Keyboard::Key key, int x, int y)=0;
	virtual sf::Vector2u& GetSize()=0;
	virtual sf::Vector2u& GetPos()=0;
	virtual ~IControl()
	{
	}
};

#endif /* ICONTROL_H_ */
