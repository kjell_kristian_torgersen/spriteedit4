/*
 * ICommand.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef ICOMMAND_H_
#define ICOMMAND_H_

#include "Bitmap.h"

class ICommand {
public:
	virtual void Execute(Bitmap& bmp)=0;
	virtual ~ICommand()
	{
		// TODO Auto-generated destructor stub
	}
};

#endif /* ICOMMAND_H_ */
