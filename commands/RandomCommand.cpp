/*
 * RandomCommand.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "RandomCommand.h"
#include <random>
RandomCommand::RandomCommand(int color) :
		color(color)
{
	std::random_device rd;
	rng.seed(rd());
}

void RandomCommand::Execute(Bitmap& bmp)
{
	bmp.Set();
	for (unsigned y = 0; y < bmp.getH(); y++) {
		for (unsigned x = 0; x < bmp.getW(); x++) {
			if (rng() & 1) {
				bmp(x, y) =color;
			}
		}
	}
}
