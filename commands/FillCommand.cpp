/*
 * FillCommand.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "FillCommand.h"

void FillCommand::FloodFill(Bitmap& bmp, int x, int y, int from, int to)
{
	if (x < 0 || x >= bmp.getW())
		return;
	if (y < 0 || y >= bmp.getH())
		return;
	if (from == to)
		return;
	if (bmp(x, y) == from) {
		bmp(x, y) = to;
		FloodFill(bmp, x, y - 1, from, to);
		FloodFill(bmp, x, y + 1, from, to);
		FloodFill(bmp, x - 1, y, from, to);
		FloodFill(bmp, x + 1, y, from, to);
	}
}

FillCommand::FillCommand(int x, int y, int color) :
		x(x), y(y), color(color)
{
}

void FillCommand::Execute(Bitmap& bmp)
{
	bmp.Set();
	FloodFill(bmp, x, y, bmp(x, y), color);
}

FillCommand::~FillCommand()
{
}
