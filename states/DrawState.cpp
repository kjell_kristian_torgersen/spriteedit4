/*
 * DrawState.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "DrawState.h"
#include <stdio.h>
#include <vector>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string>
#include "Canvas.h"
#include "SButton.h"

DrawState::DrawState(Bitmap& bmp) :
		size(1024, 768), pos(64, 64)
{
	controls.push_back(new Canvas( { pos.x, pos.y }, bmp));
	controls.push_back(new SButton( { pos.x, pos.y + 32 * 16 }, { 96, 32 }, "Test"));
	font.loadFromFile("Px437_IBM_VGA8.ttf");
	txt.setFont(font);
	txt.setColor(sf::Color::Black);
	txt.setString("Hello World!");
}

void DrawState::Draw(sf::RenderWindow& window)
{

	window.clear(sf::Color(192, 192, 192));

	for (auto control : controls) {
		control->Draw(window);
	}

	if (mouse.x < 32 && mouse.y < 32) {
		txt.setString(std::to_string(mouse.x) + ", " + std::to_string(mouse.y));
		window.draw(txt);
	}
}

void DrawState::MouseClicked(int x, int y)
{

	for (auto control : controls) {
		control->MouseClicked(x, y);
	}
}

void DrawState::MouseReleased(int x, int y)
{
	for (auto control : controls) {
		control->MouseReleased(x, y);
	}
}

void DrawState::MouseMoved(int x, int y)
{
	for (auto control : controls) {
		control->MouseMoved(x, y);
	}
	mouse.x = (x - pos.x) / 16;
	mouse.y = (y - pos.y) / 16;
}

void DrawState::KeyPressed(sf::Keyboard::Key key, int x, int y)
{
	for (auto control : controls) {
		control->KeyPressed(key, x, y);
	}
}

void DrawState::KeyReleased(sf::Keyboard::Key key, int x, int y)
{
	for (auto control : controls) {
		control->KeyReleased(key, x, y);
	}
}

sf::Vector2u& DrawState::GetSize()
{
	return size;
}

sf::Vector2u& DrawState::GetPos()
{
	return pos;
}

DrawState::~DrawState()
{
	for (auto control : controls) {
		delete control;
	}
}

