/*
 * SelectSprite.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "SelectSprite.h"
#include "DrawState.h"

#include <iostream>

SelectSprite::SelectSprite(SContext& ctx, sf::Vector2u pos) :
		pos(pos), size(32 * 16, 32 * 16), ctx(ctx), update(true)
{

}

void SelectSprite::Draw(sf::RenderWindow& window)
{
	if (update) {
		sf::Image all;
		all.create(512, 512);

		for (int y = 0; y < 16; y++) {
			for (int x = 0; x < 16; x++) {
				auto img = ctx.GetBitmap(16 * y + x).ToImg();
				all.copy(img, 32 * x, 32 * y);
			}
		}
		text.loadFromImage(all);
		sprite = sf::Sprite(text);
		update = false;
	}

	sf::RectangleShape rs;
	rs.setSize( { 32, 32 });
	rs.setOutlineColor(sf::Color::Black);
	rs.setFillColor(sf::Color::Transparent);
	window.draw(sprite);
	for (unsigned y = 0; y < 16; y++) {
		for (unsigned x = 0; x < 16; x++) {
			if (selected.x == x && selected.y == y) {
				rs.setOutlineColor(sf::Color::Red);
				rs.setOutlineThickness(-2);
			} else {
				rs.setOutlineThickness(-1);
				rs.setOutlineColor(sf::Color(64, 64, 64));
			}
			rs.setPosition(32 * x, 32 * y);
			window.draw(rs);
		}
	}

}

void SelectSprite::MouseClicked(int x, int y)
{
	unsigned int rx = x - pos.x;
	unsigned int ry = y - pos.y;
	unsigned int sx = rx / 32;
	unsigned int sy = ry / 32;
	if (sx < 16 && sy < 16) {
		std::cout << "Selected sprite " << sx << ", " << sy << std::endl;
		ctx.PushState(new DrawState(ctx.GetBitmap(16 * sy + sx)));
		update = true;
	}
}

void SelectSprite::MouseReleased(int x, int y)
{
}

void SelectSprite::MouseMoved(int x, int y)
{
	unsigned int rx = x - pos.x;
	unsigned int ry = y - pos.y;

	selected.x = rx / 32;
	selected.y = ry / 32;
}

void SelectSprite::KeyPressed(sf::Keyboard::Key key, int x, int y)
{
}

void SelectSprite::KeyReleased(sf::Keyboard::Key key, int x, int y)
{
}

sf::Vector2u& SelectSprite::GetSize()
{
	return size;
}

sf::Vector2u& SelectSprite::GetPos()
{
	return pos;
}

SelectSprite::~SelectSprite()
{
	// TODO Auto-generated destructor stub
}

