/*
 * SContext.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "SContext.h"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <stack>
#include "Bitmap.h"
#include "IControl.h"
#include "DrawState.h"
#include "SelectSprite.h"

void FILE_WriteBytes(char * path, const void * buf, int n)
{
	FILE * f = fopen(path, "wb");
	if (f) {
		fwrite(buf, 1, n, f);
		fclose(f);
	}
}

std::vector<char> FILE_ReadBytes(char * path)
{
	FILE * f = fopen(path, "rb");
	if (f) {
		fseek(f, 0, SEEK_END);
		int n = ftell(f);
		fseek(f, 0, SEEK_SET);
		std::vector<char> v(n);
		fread(&v.at(0), 1, n, f);
		fclose(f);
		return v;
	}
	return std::vector<char>();
}

SContext::SContext() :
		window(sf::VideoMode(1024, 768), "My window"), bmp(256)
{
	window.setFramerateLimit(60); // call it once, after creating the window
	FILE * f = fopen("bitmap2.bin", "rb");

	if (f) {
		for (auto& b : bmp) {
			b.Read(f);
		}
		fclose(f);
	}

	//auto pixels = FILE_ReadBytes((char*) "bitmap.bin");

	/*if (pixels.size() >= 32 * 32) {
	 bmp.push_back(Bitmap(32, 32, pixels));
	 } else {
	 bmp.push_back(Bitmap(32, 32));
	 }
	 for (int i = 0; i < 255; i++) {
	 bmp.push_back(Bitmap(32, 32));
	 }*/

	//states.push(new DrawState(bmp));
	states.push(new SelectSprite(*this, { 0, 0 }));
}

void SContext::Run()
{

	// run the program as long as the window is open
	while (window.isOpen()) {
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event)) {
			switch (event.type) {
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::MouseButtonPressed: {
				switch (event.mouseButton.button) {
				case sf::Mouse::Button::Left: {
					sf::Vector2f worldPos = window.mapPixelToCoords( { event.mouseButton.x, event.mouseButton.y });
					states.top()->MouseClicked(worldPos.x, worldPos.y);
				}
					break;
				case sf::Mouse::Button::Middle:
					break;
				case sf::Mouse::Button::Right: {
					sf::Vector2f worldPos = window.mapPixelToCoords( { event.mouseButton.x, event.mouseButton.y });
					states.top()->GetPos().x = worldPos.x;
					states.top()->GetPos().y = worldPos.y;
				}
					break;
				default:
					break;
				}

			}
				break;
			case sf::Event::MouseButtonReleased: {
				sf::Vector2f worldPos = window.mapPixelToCoords( { event.mouseButton.x, event.mouseButton.y });
				states.top()->MouseReleased(worldPos.x, worldPos.y);
			}
				break;
			case sf::Event::MouseMoved: {
				sf::Vector2f worldPos = window.mapPixelToCoords( { event.mouseMove.x, event.mouseMove.y });
				states.top()->MouseMoved(worldPos.x, worldPos.y);
			}
				break;
			case sf::Event::KeyPressed: {
				if (event.key.code == sf::Keyboard::Escape) {
					if (states.size() > 1) {
						Save();
						delete states.top();
						states.pop();
					} else {
						window.close();
					}
				} else {
					sf::Vector2f worldPos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
					states.top()->KeyPressed(event.key.code, worldPos.x, worldPos.y);
				}
			}
				break;
			default:
				break;
			}
		}
		window.clear(sf::Color(128, 128, 128));
		states.top()->Draw(window);
		window.display();
	}
}

void SContext::PushState(IControl* state)
{
	states.push(state);
}

Bitmap& SContext::GetBitmap(int n)
{
	return bmp.at(n);
}

void SContext::Save()
{
	FILE * f = fopen("bitmap2.bin", "wb");
	if (f) {
		for (auto& b : bmp) {
			b.Write(f);
		}
		fclose(f);
	}
}

SContext::~SContext()
{
	while (!states.empty()) {
		delete states.top();
		states.pop();
	}
	Save();
	//FILE_WriteBytes((char*) "bitmap.bin", &bmp[0](0, 0), 32 * 32);
}

