/*
 * RectBrush.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "RectBrush.h"

void RectBrush::SetColor(unsigned int color)
{
	this->color = color;
}

void RectBrush::Draw(unsigned int x0, unsigned int y0)
{
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			unsigned px = x0 + x - w / 2;
			unsigned py = y0 + y - h / 2;
			if (px < bmp.getW() && py < bmp.getH()) {
				bmp(px, py) = color;
			}
		}
	}
}

RectBrush::RectBrush(Bitmap& bmp, int w, int h) :
		bmp(bmp), w(w), h(h), color(0)
{
}

RectBrush::~RectBrush()
{
	// TODO Auto-generated destructor stub
}

