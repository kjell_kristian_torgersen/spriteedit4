//============================================================================
// Name        : spriteedit4.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "SContext.h"
#include <stdio.h>

static unsigned int colors[] = { 0x000000FF, 0x0000AAFF, 0x00AA00FF, 0x00AAAAFF, 0xAA0000FF, 0xAA00AAFF, 0xAA5500FF, 0xAAAAAAFF, 0x555555FF, 0x5555FFFF, 0x55FF55FF, 0x55FFFFFF,
		0xFF5555FF, 0xFF55FFFF, 0xFFFF55FF, 0xFFFFFFFF };

static sf::Color palette[17];

sf::Color GetSfColor(unsigned int n)
{
	if (n < 17)
		return palette[n];
	else
		return sf::Color(0, 0, 0, 0);
}

int main()
{
	for (int i = 0; i < 17; i++) {
		palette[i] = sf::Color(colors[i]);
	}

	SContext ctx;

	ctx.Run();

	return 0;
}
