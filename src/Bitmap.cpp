/*
 * Bitmap.cpp
 *
 *  Created on: Apr 19, 2019
 *      Author: kjell
 */

#include "Bitmap.h"
#include "global.h"
#include <iostream>

Bitmap::Bitmap(int w, int h) :
		w(w), h(h)
{
	pixels.push(std::vector<char>(w * h));
	for (int i = 0; i < w * h; i++) {
		pixels.top().at(i) = 17;
	}

}

char& Bitmap::operator ()(int x, int y)
{
	if (x >= 0 && x < w && y >= 0 && y < h) {
		return pixels.top().at(w * y + x);
	} else {
		std::cerr << "Error accessing bitmap: " << x << ", " << y << std::endl;
		return pixels.top().at(0);
	}
}

unsigned Bitmap::getH() const
{
	return h;
}

Bitmap::Bitmap(int w, int h, std::vector<char> pixels) :
		w(w), h(h)
{
	this->pixels.push(pixels);
}

Bitmap::Bitmap() :
		w(0), h(0)
{
	this->pixels.push(std::vector<char>());
}

void Bitmap::Clear(int color)
{
	pixels.push(pixels.top());
	for (unsigned int i = 0; i < pixels.top().size(); i++) {
		pixels.top().at(i) = color;
	}
}

unsigned Bitmap::getW() const
{
	return w;
}

Bitmap::~Bitmap()
{
	// TODO Auto-generated destructor stub
}

void Bitmap::Undo()
{
	if (pixels.size() > 1)
		pixels.pop();
}

void Bitmap::Set()
{
	pixels.push(pixels.top());
}

void Bitmap::Offset(int dx, int dy)
{
	auto offset = pixels.top();
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			int px = (x + dx + w) % w;
			int py = (y + dy + h) % h;
			offset.at(32 * y + x) = pixels.top().at(32 * py + px);
		}
	}
	pixels.push(offset);
}

void Bitmap::Write(FILE* f)
{
	fwrite(&w, 4, 1, f);
	fwrite(&h, 4, 1, f);
	fwrite(&pixels.top().at(0), 1, w * h, f);

}

void Bitmap::Read(FILE* f)
{
	fread(&w, 4, 1, f);
	fread(&h, 4, 1, f);
	//std::vector<char> img(w * h);
	pixels.top().resize(w * h);
	fread(&pixels.top().at(0), 1, w * h, f);
}

sf::Image Bitmap::ToImg()
{
	sf::Image img;
	std::vector<sf::Uint8> pixels(w * h * 4);
	//sf::Texture txt;

	for (int i = 0; i < w * h; i++) {
		int x = i % w;
		int y = i / w;
		auto col = GetSfColor((int) operator()(x, y));
		pixels.at(4 * i + 0) = col.r;
		pixels.at(4 * i + 1) = col.g;
		pixels.at(4 * i + 2) = col.b;
		pixels.at(4 * i + 3) = col.a;
	}

	img.create(w, h, &pixels.at(0));
	//txt.loadFromImage(img);
	//sf::Sprite sprite(txt);
	return img;
}

void Bitmap::Double()
{
	auto copy = pixels.top();
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			int hx = x / 2;
			int hy = y / 2;
			copy.at(w * y + x) = pixels.top().at(w * hy + hx);
		}
	}
	pixels.push(copy);
}

std::vector<char> Bitmap::Copy()
{
	return pixels.top();
}

void Bitmap::Paste(std::vector<char> img)
{
	pixels.push(img);
}
