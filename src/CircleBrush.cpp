/*
 * CircleBrush.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#include "CircleBrush.h"
#include "Bitmap.h"

CircleBrush::CircleBrush(Bitmap& bmp, int r) :
		bmp(bmp), r(r), color(0)
{
}

void CircleBrush::SetColor(unsigned int color)
{
	this->color = color;
}

void CircleBrush::Draw(unsigned int x0, unsigned int y0)
{
	for (int y = -r; y < r; y++) {
		for (int x = -r; x < r; x++) {
			unsigned px = x0 + x;
			unsigned py = y0 + y;
			if (x * x + y * y < r * r) {
				if (px < bmp.getW() && py < bmp.getH()) {
					bmp(px, py) = color;
				}
			}
		}
	}
}

CircleBrush::~CircleBrush()
{
	// TODO Auto-generated destructor stub
}

