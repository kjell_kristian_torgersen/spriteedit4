/*
 * DrawState.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef DRAWSTATE_H_
#define DRAWSTATE_H_

#include <vector>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "IControl.h"
#include "Bitmap.h"

class DrawState: public IControl {


	std::vector<IControl*> controls;

	sf::Vector2u size;
	sf::Vector2u pos;
	sf::Font font;
	sf::Text txt;
	sf::Vector2u mouse;
public:
	DrawState(Bitmap& bmp);
	virtual void Draw(sf::RenderWindow& window);
	virtual void MouseClicked(int x, int y);
	virtual void MouseReleased(int x, int y);
	virtual void MouseMoved(int x, int y);
	virtual void KeyPressed(sf::Keyboard::Key key, int x, int y);
	virtual void KeyReleased(sf::Keyboard::Key key, int x, int y);
	virtual sf::Vector2u& GetSize();
	virtual sf::Vector2u& GetPos();
	virtual ~DrawState();
};

#endif /* DRAWSTATE_H_ */
