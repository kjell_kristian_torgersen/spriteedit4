/*
 * RandomCommand.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef RANDOMCOMMAND_H_
#define RANDOMCOMMAND_H_

#include <random>
#include <ICommand.h>
#include "Bitmap.h"

class RandomCommand: public ICommand {
	int color;
	std::default_random_engine rng;
public:
	RandomCommand(int color);
	virtual void Execute(Bitmap&bmp);
	virtual ~RandomCommand()
	{
		// TODO Auto-generated destructor stub
	}
};

#endif /* RANDOMCOMMAND_H_ */
