/*
 * CircleBrush.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef CIRCLEBRUSH_H_
#define CIRCLEBRUSH_H_

#include <IBrush.h>
#include "Bitmap.h"

class CircleBrush: public IBrush {
	Bitmap & bmp;
	int r;
	int color;
public:
	CircleBrush(Bitmap& bmp, int r);
	virtual void SetColor(unsigned int color);
	virtual void Draw(unsigned int x, unsigned int y);
	virtual ~CircleBrush();
};

#endif /* CIRCLEBRUSH_H_ */
