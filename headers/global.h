/*
 * global.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <SFML/Graphics.hpp>

sf::Color GetSfColor(unsigned int n);

#endif /* GLOBAL_H_ */
