/*
 * SelectSprite.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef SELECTSPRITE_H_
#define SELECTSPRITE_H_

#include "IControl.h"
#include "SContext.h"

class SelectSprite: public IControl {
	sf::Vector2u pos;
	sf::Vector2u size;
	sf::Vector2u selected;
	SContext& ctx;
	sf::Image img;
	sf::Texture text;
	sf::Sprite sprite;
	bool update;
public:
	SelectSprite(SContext& ctx, sf::Vector2u pos);
	virtual void Draw(sf::RenderWindow& window);
	virtual void MouseClicked(int x, int y);
	virtual void MouseReleased(int x, int y);
	virtual void MouseMoved(int x, int y);
	virtual void KeyPressed(sf::Keyboard::Key key, int x, int y);
	virtual void KeyReleased(sf::Keyboard::Key key, int x, int y);
	virtual sf::Vector2u& GetSize();
	virtual sf::Vector2u& GetPos();
	virtual ~SelectSprite();
};

#endif /* SELECTSPRITE_H_ */
