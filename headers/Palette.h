/*
 * Palette.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef PALETTE_H_
#define PALETTE_H_

#include <IControl.h>
#include <array>

class Palette: public IControl {
	sf::Vector2u pos;
	sf::Vector2u size;
	int drawColor;

public:
	Palette(sf::Vector2u pos);
	virtual void Draw(sf::RenderWindow& window);
	virtual void MouseClicked(int x, int y);
	virtual void MouseReleased(int x, int y);
	virtual void MouseMoved(int x, int y);
	virtual void KeyPressed(sf::Keyboard::Key key, int x, int y);
	virtual void KeyReleased(sf::Keyboard::Key key, int x, int y);
	virtual sf::Vector2u& GetSize();
	virtual sf::Vector2u& GetPos();
	virtual ~Palette();
	int GetColor();
};

#endif /* PALETTE_H_ */
