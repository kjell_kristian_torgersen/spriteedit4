/*
 * Canvas.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef CANVAS_H_
#define CANVAS_H_

#include "IControl.h"
#include "ICommand.h"
#include "Bitmap.h"
#include "Palette.h"
#include "IBrush.h"
#include <random>

class Canvas: public IControl {
	sf::Vector2u pos;
	sf::Vector2u size;
	bool drawing;
	std::vector<sf::RectangleShape> rss;
	Palette palette;
	Bitmap& bmp;
	std::vector<char> copy;
	IBrush* brush;

public:
	Canvas(sf::Vector2u pos, Bitmap& bmp);
	virtual void Draw(sf::RenderWindow& window);
	virtual void MouseClicked(int x, int y);
	virtual void MouseReleased(int x, int y);
	virtual void MouseMoved(int x, int y);
	virtual void KeyPressed(sf::Keyboard::Key key, int x, int y);
	virtual void KeyReleased(sf::Keyboard::Key key, int x, int y);
	virtual sf::Vector2u& GetSize();
	virtual sf::Vector2u& GetPos();
	void SetBrush(IBrush * brush);
	void Execute(ICommand * cmd);
	virtual ~Canvas();
};

#endif /* CANVAS_H_ */
