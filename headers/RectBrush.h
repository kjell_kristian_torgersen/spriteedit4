/*
 * RectBrush.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef RECTBRUSH_H_
#define RECTBRUSH_H_

#include <IBrush.h>
#include "Bitmap.h"

class RectBrush: public IBrush {
	Bitmap & bmp;
	int w;
	int h;
	int color;
public:
	RectBrush(Bitmap& bmp, int w, int h);
	virtual void SetColor(unsigned int color);
	virtual void Draw(unsigned int x, unsigned int y);
	virtual ~RectBrush();
};

#endif /* RECTBRUSH_H_ */
