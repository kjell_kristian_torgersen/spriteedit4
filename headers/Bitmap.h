/*
 * Bitmap.h
 *
 *  Created on: Apr 19, 2019
 *      Author: kjell
 */

#ifndef BITMAP_H_
#define BITMAP_H_

#include <vector>
#include <stack>
#include <stdio.h>
#include <SFML/Graphics.hpp>

class Bitmap {
	int w;
	int h;
	//std::vector<char> pixels;
	std::stack<std::vector<char>> pixels;
public:
	Bitmap();
	Bitmap(int w, int h);
	Bitmap(int w, int h, std::vector<char> pixels);
	void Clear(int color);
	char & operator()(int x, int y);
	virtual ~Bitmap();
	unsigned getH() const;
	unsigned getW() const;
	void Offset(int dx, int dy);
	void Undo();
	void Set();
	void Write(FILE * f);
	void Read(FILE * f);
	void Double();
	std::vector<char> Copy();
	void Paste(std::vector<char> img);
	sf::Image ToImg();
};

#endif /* BITMAP_H_ */
