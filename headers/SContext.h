/*
 * SContext.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef SCONTEXT_H_
#define SCONTEXT_H_

#include "Bitmap.h"
#include "IControl.h"

class SContext {
	std::stack<IControl*> states;
	sf::RenderWindow window;
	std::vector<Bitmap> bmp;
public:
	SContext();
	void Run();
	void PushState(IControl * state);
	Bitmap& GetBitmap(int n);
	void Save();
	virtual ~SContext();
};

#endif /* SCONTEXT_H_ */
