/*
 * SButton.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef SBUTTON_H_
#define SBUTTON_H_

#include <SFML/Graphics.hpp>
#include <IControl.h>
#include <string>

class SButton: public IControl {
	sf::Vector2u pos;
	sf::Vector2u size;
	std::string text;
	sf::RectangleShape rs;
public:
	SButton(sf::Vector2u pos, sf::Vector2u size, std::string text);
	virtual void Draw(sf::RenderWindow& window);
	virtual void MouseClicked(int x, int y);
	virtual void MouseReleased(int x, int y);
	virtual void MouseMoved(int x, int y);
	virtual void KeyPressed(sf::Keyboard::Key key, int x, int y);
	virtual void KeyReleased(sf::Keyboard::Key key, int x, int y);
	virtual sf::Vector2u& GetSize();
	virtual sf::Vector2u& GetPos();
	virtual ~SButton()
	{
		// TODO Auto-generated destructor stub
	}
};

#endif /* SBUTTON_H_ */
