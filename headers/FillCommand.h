/*
 * FillCommand.h
 *
 *  Created on: Apr 20, 2019
 *      Author: kjell
 */

#ifndef FILLCOMMAND_H_
#define FILLCOMMAND_H_
#include "Bitmap.h"
#include <ICommand.h>

class FillCommand: public ICommand {
	int x;
	int y;
	int color;
	void FloodFill(Bitmap& bmp, int x, int y, int from, int to);
public:
	FillCommand(int x, int y, int color);
	virtual void Execute(Bitmap& bmp);
	virtual ~FillCommand();
};

#endif /* FILLCOMMAND_H_ */
